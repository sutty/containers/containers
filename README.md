This is a meta-repository to build Sutty's containers.  We don't have
a Docker registry because it uses too much disk space.  Instead, we
build containers locally and push them over SSH to the servers.

# Build

Run `make container_name` to build a container and push it to servers.
Add `k=true` to keep it locally (your Docker pruning needs to skip
images tagged with `keep`).

# Conventions

Each container has the name of the container.  The distribution is
Alpine to save on resources.  We use Monit to supervise services, it can
also run scripts every given amount of cycles or cron-like.

To start a container, run `make new b=container_name`.  It will create
a new repository based on the `skel`.

If a service needs to be supervised, add a `monit.conf` file with the
Monit configuration.

If it needs a script, add a `file.sh` script and don't forget to install
it on the `Dockerfile`.

There's no need to change the `ENTRYPOINT`, if you're using the Monit
container, it will init with `tini` and run `monit`, which in turn will
run your services.

# Building packages

We don't compile packages on containers.  That's for package managers.
Check the `repository` container and the `sports` repo for our
APKBUILDs.

# Upgrading

Check the `order` variable on `Makefile` and edit those containers.  If
we're lucky, most of the changes only require to update Alpine version
