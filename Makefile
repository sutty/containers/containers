.SHELL = /bin/bash
.DEFAULT_GOAL := help

# Container names
containers := $(patsubst ./%/Dockerfile,%,$(wildcard ./*/Dockerfile))

# Upgrade order
order := monit sdk sdk-ruby gem-compiler repository

# Keep locally
k ?= false
# Servers
h ?= anarres.sutty.nl athshe.sutty.nl
# Container name
b ?= $(shell readlink -f $(PWD)/../ | xargs basename)
# Version
v ?= $(shell date +%F | tr "-" ".")
# Tag
n := $(b)/$(d):$(v)
# Push to remote servers
s ?= true
# Append Alpine version
a ?= 3.13

# Meta rule to build individual containers
.PHONY: $(containers)
$(containers):
	$(MAKE) all d=$@

everything: $(order)

build: ## Build container
	docker build -t $n $d/

tag: ## Tag container
	docker tag $n $b/$d:latest
	docker tag $n $b/$d:$a
	$k && docker tag $n $b/$d:keep ; :

save: ## Push to servers
	$s && for h in $h; do \
		docker save $n | gzip | ssh root@$$h docker load ; \
		ssh root@$$h docker tag $n $b/$d:latest ; \
	done ; :

new: ## Start container
	test -n "$b"
	! test -d "$b"
	git clone git@0xacab.org:sutty/containers/skel.git $b
	cd $b && git remote rename origin upstream
	cd $b && git remote add origin git@0xacab.org:sutty/containers/$b.git
	cd $b && git push -u origin antifascista

all: build tag save

help:
	@echo "make [all|build|tag|save] d=directorio [v=version] [k=false]"
	@echo "\`make CONTAINER\` equals to \`make all d=CONTAINER\`"
